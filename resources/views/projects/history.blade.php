@extends('layouts.app')

@section('htmlheader_title')
  Project Index
@endsection

@section('contentheader_title')
  
@endsection


@section('main-content')

  <div class="col-md-12">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Existing Project</h3>
            </div>

                <div class="box-body">
                     
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  Current Projects
                              </div>

                      <div class="panel-body">
                           <table class="table table-striped task-table">

                    
                        <thead>
                            <th>Project Name</th>
                            <th>Created_at</th>
                            <th>FTP</th>
                            <th>Action</th>
                        </thead>

                        <tbody>
                          @foreach($project->history as $projectBackup)
                            <tr>
                            <td>{{$project->name}}</td>
                            <td>{{$projectBackup->created_at}}</td>
                            <td>
                              @if($projectBackup->ftp)
                                <span class="label label-info">FTP</span>
                              @else
                                <span class="label label-default">LOCAL</span>
                              @endif

                            </td>
                            <td>
                              <a href="{{route('project.backup.restore',$projectBackup->id)}}" class="btn btn-xs btn-primary">Restore</a>
                            </td>
                            </tr>
                          @endforeach
                        </tbody>
                  </table>
                      </div>
                          </div>
                </div>
    </div>
  </div>
                    
@endsection