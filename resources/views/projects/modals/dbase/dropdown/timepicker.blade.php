<div class="input-group clockpicker" id="time">
    <input type="text" class="form-control" value="09:30">
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
</div>

@push('js')
<script src="{{asset('js/project/dbase/create.js')}}" type="text/javascript"></script>
@endpush