<div class="example-modal"  >
  <div class="modal modal-primary fade" id="editFolderModal" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Primary Modal</h4>
        </div>
        <div class="modal-body">
            <div class="form-group ">
                <label for="name">Name : </label>
                <input type="text" id="editFolderModalName" name="name" class="form-control"  placeholder="Name">
            </div>
            
            <div class="form-group ">
                <label for="path">Path : </label>
                <input type="text" id="editFolderModalPath" name="path" class="form-control"  placeholder="specify the directory path">
            </div>

            <input type="hidden" name="itemId" id="folderHiddenId">
            <div class="form-group ">
                <label for="desc">Description : </label>
                <textarea id="editFolderModalDesc" placeholder="add some description" name="desc" class="form-control"></textarea>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="button" id="editFolderSubmit" class="btn btn-outline">update</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div>