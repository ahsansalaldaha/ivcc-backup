<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TheLastTimeProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('time_period');
            $table->time('time_picked');
            $table->string('dayName')->nullable();
            $table->integer('date_of_Month')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('time_period');
            $table->dropColumn('time_picked');
            $table->dropColumn('dayName');
            $table->dropColumn('date_of_Month');
        });
    }
}
