<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_backups', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('project_id')->unsigned()->change();
            // $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('path');
            $table->boolean('ftp');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_backups');
    }
}
