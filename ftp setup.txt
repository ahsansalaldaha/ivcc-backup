https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-on-ubuntu-12-04
http://www.liquidweb.com/kb/how-to-install-and-configure-vsftpd-on-ubuntu-14-04-lts/

sudo apt-get install vsftpd

sudo nano /etc/vsftpd.conf

anonymous_enable=NO

Prior to this change, vsftpd allowed anonymous, unidentified users to access the server's files. This is useful if you are seeking to distribute information widely, but may be considered a serious security issue in most other cases.
After that, uncomment the local_enable option, changing it to yes and, additionally, allow the user to write to the directory.

local_enable=YES
write_enable=YES

Finish up by uncommenting command to chroot_local_user. When this line is set to Yes, all the local users will be jailed within their chroot and will be denied access to any other part of the server.

chroot_local_user=YES
Save and Exit that file.

Create a new directory within the user's home directory
mkdir /home/username

Change the ownership of that file to root
chown root:root /home/username
chmod a-w /home/username/
mkdir /home/username/files
chown username:username /home/username/files/

Create user with 
useradd [username]
Create user's password with 
passwd [username]. (You'll be prompted to specify the password).

nano /etc/vsftpd.conf
Then make the following change

pam_service_name=ftp

sudo service vsftpd restart
