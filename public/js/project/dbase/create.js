$(document).ready(function () {
    
    $('#createDbaseSubmit').click(function() {
        var name = $('#dbmodalName').val();
        var desc = $('#dbmodalDesc').val();

       db.addItem(name,desc);
       $('#createDbaseModal').modal('hide');
    });

    $('#editDbSubmit').click (function(){
        var n_name = $('#editDbModalName').val();
        var n_desc = $('#editDbModalDesc').val();
        var index = $('#DbhiddenId').val();
        db.replace (n_name,n_desc, index);

    });
    


});

class Dbase {
    constructor() {
        this.items = [];
    }

    addItem(name,desc){
        var obj = new Object();
        obj.name = name;
        obj.desc = desc;

        this.items.push(obj);
        this.populateTable();
    }


    


    remove(itemIndex, that){
        this.items.splice(itemIndex,1);
        this.populateTable();
    }

    edit(itemIndex, that){
        $('#editDbModalName').val(this.items[itemIndex].name);
        $('#editDbModalDesc').val(this.items[itemIndex].desc);
        $('#DbhiddenId').val(itemIndex);
        $('#editDbaseModal').modal('show');

    }

     edit(itemIndex, that){
        $('#editDbModalName').val(this.items[itemIndex].name);
        $('#editDbModalDesc').val(this.items[itemIndex].desc);
        $('#DbhiddenId').val(itemIndex);
        $('#editDbaseModal').modal('show');

    }

    replace(name, desc, index){
        var obj1 = new Object();
        obj1.name = name;
        obj1.desc = desc;
        this.items[index] = obj1;
        this.populateTable();
        $('#editDbaseModal').modal('hide');
    }

    populateTable(){
        if (this.items.length<1) {
            var strVar = "<div class=\"well text-center\">No Databases found.</div>";
            $('#dbaseTable').html(strVar);
            return;
        };
        var strVar="";
        strVar += "<div class=\"table-responsive\">";
        strVar += "<table class=\"table table-responsive table-bordered table-striped datatable\">";
        strVar += "    <thead>";
        strVar += "    <th>Name<\/th>";
        strVar += "    <th>Description<\/th>";
        strVar += "    <th width=\"50px\">Action<\/th>";
        strVar += "    <\/thead>";
        strVar += "    <tbody>";
        for(var item in this.items){
            strVar += "        <tr>";
            strVar += "            <td><span class=\"text-primary\">"+this.items[item].name+"<\/span><\/td>";
            strVar += "            <td>"+this.items[item].desc+"<\/td>";
            strVar += "            <td>";
            strVar += "             <a href=\"#\" onclick=\"db.edit("+item+",this)\"><i class=\"glyphicon glyphicon-edit\"><\/i><\/a>"
            strVar += "             <a href=\"#\" onclick=\"db.remove("+item+",this)\"><i class=\"glyphicon glyphicon-trash\"><\/i><\/a>"
            strVar += "            <\/td>";
            strVar += "        <\/tr>";
        }

        strVar += "    <\/tbody>";
        strVar += "<\/table><\/div>";

        $('#dbaseTable').html(strVar);

    }





}

var db = new Dbase();