<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use App\Project;


use Carbon\Carbon;
use DateTime;
use App\Backman;

class CleanUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geekinn:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $projects = Project::where('status','=','pending')->with('folders','dbases')->get();
        foreach ($projects as $project) {
            $this->cleanUp($project);
        }

    }
    public function cleanUp($project)
    {
        $date = new DateTime();
        Log::info('CleanUp of project id '.$project->id.' at '.$date->format('Y-m-d H:i:s'));

        $backman = new Backman();
        $backman->populateFromProject($project);
        $path = $backman->cleanup();
    }
}
