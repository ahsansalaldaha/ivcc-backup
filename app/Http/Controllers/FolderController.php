<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Folder;


class FolderController extends Controller
{
    public function check()
    {
        return view('projects.create');
    }

    public function index()
    {
        $folders = Folder::orderBy('project_id', 'asc')->get();

        return view('projects.index', [
        'folders' => $folders]);

        // return view('folders.index', [
        // 'folders' => $folders]);
    }
}
