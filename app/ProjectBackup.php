<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectBackup extends Model
{
    protected $table =  "project_backups";

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
